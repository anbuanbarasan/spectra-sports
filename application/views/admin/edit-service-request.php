<?php $service_id = $records[0]->service; ?>
<div class="content-wrapper">
    
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
              <div class="box">
				<form class="update_data">
					<div class="box-body table-responsive">
					  <table class="table table-hover">
						<tbody>
							<?php
							if(!empty($records))
							{
								$inc = 1;
								foreach($records as $record)
								{
							?>
								<tr>
									<th>Registered Date</th>
									<td><?php echo date("d M, Y", strtotime($record->date_time))?></td>
								</tr>
								<tr>
									<th>User</th>
									<td>
										<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "first_name")); ?>
										<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "last_name")); ?>
										<br>
										<b>(<?php echo ucfirst($this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "phone_number")); ?>)</b>
										<br>
										<?php $ser = $this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "service"); ?>
										<b>(<?=$this->common_model->get_record("tbl_services", "id=" . $ser, "name"); ?>)</b>
									</td>
								</tr>
								<tr>
									<th>Name</th>
									<td>
										<input type="text" value="<?php echo $record->name?>" name="name" required class="form-control">
										<input type="hidden" value="tbl_service_request" name="table_name">
										<input type="hidden" value="<?=$record->id?>" name="row_id">
									</td>
								</tr>
								<tr>
									<th>Date of birth</th>
									<td>
										<input type="date" value="<?php echo $record->date_of_birth?>" name="date_of_birth" required class="form-control">
									</td>
								</tr>
								<tr>
									<th>Gender</th>
									<td><?php echo $record->gender ?></td>
								</tr>
								<tr>
									<th>Sport</th>
									<td><?php echo $this->common_model->get_record("tbl_sports", "id=" . $record->sport, "name"); ?></td>
								</tr>
								<tr>
									<th>Address</th>
									<td>
										<input type="text" value="<?php echo $record->address?>" name="address" required class="form-control">
									</td>
								</tr>
								<tr>
									<th>City</th>
									<td>
										<input type="text" value="<?php echo $record->city?>" name="city" required class="form-control">
									</td>
								</tr>
								<tr>
									<th>State</th>
									<td>
										<input type="text" value="<?php echo $record->state?>" name="state" required class="form-control">
									</td>
								</tr>
								<tr>
									<th>Pincode</th>
									<td>
										<input type="text" value="<?php echo $record->pincode?>" name="pincode" required class="form-control">
									</td>
								</tr>
								<tr>
									<th>Designation</th>
									<td>
										<input type="text" value="<?php echo $record->designation?>" name="designation" required class="form-control">
									</td>
								</tr>
								<tr>
									<th>Name of facility</th>
									<td>
										<input type="text" value="<?php echo $record->name_of_facility?>" name="name_of_facility" required class="form-control">
									</td>
								</tr>
								<tr>
									<th>Specific Requirement</th>
									<td>
										<input type="text" value="<?php echo $record->specific_requirements?>" name="specific_requirements" required class="form-control">
									</td>
								</tr>
								<tr>
								<?php if($service_id == '1'): ?>
										<th>Level of participation</th>
										<td>
											<input type="text" value="<?php echo $record->level_of_participation?>" name="level_of_participation" required class="form-control">
										</td>
									</tr>
									<tr>
								<?php endif; ?>
								<?php if($service_id == '4'): ?>
										<th>Purpose in short</th>
										<td>
											<input type="text" value="<?php echo $record->purpose?>" name="purpose" required class="form-control">
										</td>
									</tr>
									<tr>
										<th>Fund Raised</th>
										<td>
											<?php echo $record->raised_amount?>
										</td>
									</tr>
									<tr>
										<th>Fund required</th>
										<td>
											<input type="text" value="<?php echo $record->fund_required?>" name="fund_required" required class="form-control">
										</td>
									</tr>
									<tr>
										<th>Described Purpose</th>
										<td>
											<textarea class="form-control" name="described_purpose" required="" id="editor"><?php echo $record->described_purpose ?></textarea>
										</td>
									</tr>
									<tr>
								<?php endif; ?>
								<?php if($service_id == '3'): ?>
										<th>Career option</th>
										<td>
											<input type="text" value="<?php echo $record->career_option?>" name="career_option" required class="form-control">
										</td>
									</tr>
									<tr>
								<?php endif; ?>
								</tr>
								<?php if($service_id != '4'): ?>
									<tr>
										<th>Status</th>
										<td>
											<select name="request_status" class="form-control" style="width: 150px;">
												<option <?=($record->request_status == 'pending')?"selected":""?> value="pending">Pending</option>
												<option <?=($record->request_status == 'open')?"selected":""?> value="open">Open</option>
												<option <?=($record->request_status == 'reviewing')?"selected":""?> value="reviewing">Reviewing</option>
												<option <?=($record->request_status == 'resolved')?"selected":""?> value="resolved">Resolved</option>
												<option <?=($record->request_status == 'not resolved')?"selected":""?> value="not resolved">Not Resolved</option>
												<option <?=($record->request_status == 'closed')?"selected":""?> value="closed">Closed</option>
											</select>
										</td>
									</tr>
								<?php endif; ?>
								<?php if($service_id == '4'): ?>
									<tr>
										<th>Status</th>
										<td>
											<select name="request_status" class="form-control" style="width: 150px;">
												<option <?=($record->request_status == 'pending')?"selected":""?> value="pending">
													Pending
												</option>
												<option <?=($record->request_status == 'approved')?"selected":""?> value="approved">
													Approved
												</option>
												<option <?=($record->request_status == 'declined')?"selected":""?> value="declined">
													Declined
												</option>
											</select>
										</td>
									</tr>
								<?php endif; ?>
								<tr>
									<th>Fund Raiser Image</th>
									<td>
										<input type="file" name="raiser_image" class="form-control" accept="images/*">
									</td>
									<td>
										<a target="_blank" href="<?=base_url()?>uploads/service-requests/<?php echo $record->raiser_image ?>">
											<img src="<?=base_url()?>uploads/service-requests/<?php echo $record->raiser_image ?>" width="50">
										</a>
									</td>
								</tr>
								<tr>
									<th>Proof</th>
									<td>
										<input type="file" name="proof_upload" class="form-control">
									</td>
									<td>
										<a target="_blank" href="<?=base_url()?>uploads/service-requests/<?php echo $record->proof_upload ?>">Download</a>
									</td>
								</tr>
							<?php
								$inc++;
								}
							}
							?>
						</tbody>
					  </table>
					  <div class="row">
						<div class="text-right col-md-12">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					  </div>
					</div><!-- /.box-body -->
				</form>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/dist/js/ckeditor.js" charset="utf-8"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>