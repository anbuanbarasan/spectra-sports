<?php
require_once(APPPATH.'config/database_config.php');
?>

<div class="content-wrapper">
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                <div class="box box-primary">
					<div class="box-header">
                        <h3 class="box-title">Edit Idols <small>(All the fields are mandatory.)</small></h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->

                    <form  action=""     method="POST"  enctype="multipart/form-data">
                        <div class="box-body">
                              
                               

                              <input type="hidden" name="id" value="<?=$record->id?>">
                              <input type="hidden" name="image_old" value="<?=$record->image_url?>">
                              
                                      
                               <div class="row">
                                <div class="col-md-4">                                
                                    <a href="<?=base_url()?><?=$record->image_url?>" target="_blank">
										<img src="<?=base_url()?><?=$record->image_url?>" width="100" alt="<?=base_url()?><?=$record->image_url?>">
									</a>
                                </div>
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="date">Event Poster</label>
                                        <input type="file"  class="form-control" name="image" id="event_image">
                                    </div>
                                </div>
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text"   value="<?=$record-> title?>" class="form-control required" name="title">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="short_description">Short Description</label>
                                        <input type="text"   value="<?=$record->short_description?>" class="form-control required" name="short_description">
                                    </div>
                                </div>
                            </div>
                                                       
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <input type="text"  class="form-control" value="<?=$record->description?>" name="description"></input>
                                    </div>
                                </div>
                            </div>
                             
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Update" name="updatebtn"/>
                            <!-- <input type="reset" class="btn btn-default" value="Reset"/> -->
                        </div>
                    </form>
                    <?php
                    //print_r($_POST);
                     if(isset($_POST['updatebtn']))
                     {
                         $img="";
                         if(empty($_FILES['image']['name'])){
                            $img=$_POST['image_old'];                         
                         }else{
                            $img=$_FILES['image']['name'];
                            $file = $_FILES['image'];
                            $file_name_with_extenstion = $file['name'];
                            $file_name = pathinfo($file_name_with_extenstion, PATHINFO_FILENAME);
                            $extension = pathinfo($file_name_with_extenstion, PATHINFO_EXTENSION);
                            $file_tmp_location =$_FILES['image']['tmp_name'];
                            $upload_name = $file_name.time().".$extension";
                            if(move_uploaded_file($file_tmp_location,"uploads/".$upload_name)){
                                echo "The file has been uploaded.";
                            }else{
                                echo "There was an error.";
                            }
                            $img="uploads/"."$upload_name";                         
                         }
                         
                         $title = $_POST['title'];
                         $short_description = $_POST['short_description'];
                         $description = $_POST['description'];
                         $id=$_POST['id'];
                       
                       $query= "UPDATE tbl_idols SET image_url='$img', title='$title',short_description='$short_description',description='$description' WHERE id='$id'";
                       $query_run = mysqli_query($conn,$query);
                       //print_r($query);
                       if($query_run)
                       {
                            ?>

                           <div class="alert alert-success">
                           <strong>Update Success!</strong> .
                         </div>

                          <?php
                       }

                       else
                       {
                           ?>
                        <div class="alert alert-warning">
                        <strong>Warning!</strong> Not Update..
                      </div> 
                      <?php

                       }
                     }
                     ?>
                   
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>