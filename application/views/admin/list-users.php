<div class="content-wrapper">
    
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body table-responsive">
                  <table class="table table-hover data_table">
					<thead>
						<tr>
							<th>Sl. No.</th>
							<th>Name</th>
							<th>Contact</th>
							<th>Role</th>
							<?php if($this->common_model->get_record("tbl_services", "id=" . $service_id, "name") == "Academy" || $this->common_model->get_record("tbl_services", "id=" . $service_id, "name") == "University"): ?>
								<th>Organization Name</th>
							<?php endif; ?>
							<th>Registered Date</th>
							<th class="text-center">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($records))
						{
							$inc = 1;
							foreach($records as $record)
							{
						?>
						<tr>
							<td><?php echo $inc; ?></td>
							<td><?php echo ucfirst($record->first_name) ?> <?php echo ucfirst($record->last_name) ?></td>
							<td><?php echo $record->phone_number ?></td>
							<td><?php echo $this->common_model->get_record("tbl_services", "id=" . $record->service, "name"); ?></td>
							<?php if($this->common_model->get_record("tbl_services", "id=" . $service_id, "name") == "Academy" || $this->common_model->get_record("tbl_services", "id=" . $service_id, "name") == "University"): ?>
								<td><?php echo $record->organization_name; ?></td>
							<?php endif; ?>
							<td><?php echo $record->date_time; ?></td>
							<td class="text-center">
								<form class="update_data" reload-action="true">
									<?php if($record->status == 0):?>
										<input type="hidden" name="table_name" value="tbl_general_users">
										<input type="hidden" name="row_id" value="<?=$record->id?>">
										<input type="hidden" name="status" value="1">
										<button class='btn btn-sm btn-success'>Active</button>
									<?php else: ?>
										<input type="hidden" name="table_name" value="tbl_general_users">
										<input type="hidden" name="row_id" value="<?=$record->id?>">
										<input type="hidden" name="status" value="0">
										<button class='btn btn-sm btn-danger'>Inactive</button>
									<?php endif; ?>
								</form>
							</td>
						</tr>
						<?php
							$inc++;
							}
						}
						?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
