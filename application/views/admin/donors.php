<div class="content-wrapper">
    
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body table-responsive">
                  <table class="table table-hover data_table">
					<thead>
						<tr>
							<th>Sl. No.</th>
							<th>Fund Raiser</th>
							<th>User</th>
							<th>Name</th>
							<th>Amount</th>
							<th>Mobile Number</th>
							<th>Email</th>
							<th>Address</th>
							<th>City</th>
							<th>State</th>
							<th>Pincode</th>
							<th>Payment Request ID</th>
							<th>Payment ID</th>
							<th>Payment Method</th>
							<th>Status</th>
							<th>Date/Time</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($records))
						{
							$inc = 1;
							foreach($records as $record)
							{
						?>
						<tr>
							<td><?php echo $inc; ?></td>
							<td><?=$this->common_model->get_record("tbl_service_request", "id=" . $record->fund_raiser_id, "purpose")?></td>
							<td>
								<?php if($record->user_id != 0): ?>
									<?=$this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "first_name")?> 
									<?=$this->common_model->get_record("tbl_general_users", "id=" . $record->user_id, "last_name")?>
								<?php else: ?>
									Guest
								<?php endif; ?>
							</td>
							<td><?=$record->name?></td>
							<td><?=$record->amount?></td>
							<td><?=$record->phone_number?></td>
							<td><?=$record->email?></td>
							<td><?=$record->address?></td>
							<td><?=$record->city?></td>
							<td><?=$record->state?></td>
							<td><?=$record->pincode?></td>
							<td><?=$record->payment_request_id?></td>
							<td><?=$record->payment_id?></td>
							<td><?=$record->payment_method?></td>
							<td>Paid</td>
							<td><?=$record->date_time?></td>
						</tr>
						<?php
							$inc++;
							}
						}
						?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
		
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
