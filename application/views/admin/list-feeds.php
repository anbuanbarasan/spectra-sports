<div class="content-wrapper">
    
    <section class="content"> 
        <div class="row">
            <div class="col-md-12">
              <div class="box">
                <div class="box-body table-responsive">
                  <table class="table table-hover data_table">
					<thead>
						<tr>
							<th>Sl. No.</th>
							<th>Image</th>
							<th>Category</th>
							<th>Title</th>
							<th>Date</th>
							<th>Redirect Link</th>
							<th>Total Likes</th>
							<th class="text-center">Edit</th>
							<th class="text-center">Delete</th>
						</tr>
					</thead>
					<tbody>
						<?php
						if(!empty($records))
						{
							$inc = 1;
							foreach($records as $record)
							{
						?>
								<tr>
									<td><?php echo $inc; ?></td>
									<td>
										<a href="<?=base_url()?>uploads/news/<?=$record->image?>" target="_blank">
											<img width="50" src="<?=base_url()?>uploads/news/<?=$record->image?>">
										</a>
									</td>
									<td>
										<?php echo $this->common_model->get_record("tbl_news_category", "id=" . $record->category, "name") ?>
									</td>
									<td>
										<?php echo $record->title ?>
									</td>
									<td>
										<?php echo $record->date ?>
									</td>
									<td>
										<?php if($record->redirect_link != ""): ?>
											<a class="btn btn-sm btn-primary" target="_blank" href="<?=$record->redirect_link?>">
												Link
											</a>
										<?php endif; ?>
									</td>
									<td>
										<?=sizeof($this->common_model->get_records("tbl_news_likes", "status = '0' and news_id = '" . $record->id . "'"))?>
									</td>
									<td class="text-center">
										<a href="<?=base_url()?>admin/edit-feed/<?=$record->id?>" class="btn btn-sm btn-warning">Edit</a>
									</td>							
									<td class="text-center">								
										<form class="update_data update_data_<?=$record->id?>" this_id="form-<?=uniqid()?>" reload-action="true">
											<input type="hidden" name="table_name" value="tbl_news">
											<input type="hidden" name="row_id" value="<?=$record->id?>">
											<input type="hidden" name="status" value="1">
											<button class="btn btn-sm btn-danger" type="submit">Delete</button>
										</form>
									</td>
								</tr>
						<?php
							$inc++;
							}
						}
						?>
					</tbody>
                  </table>
                  
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

	
		
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/common.js" charset="utf-8"></script>
