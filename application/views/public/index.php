<!-- ======= Hero Section ======= -->
<?php 
require_once(APPPATH.'config/database_config.php');
?>
<section id="hero" class="d-flex flex-column justify-content-center align-items-center">
   <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
         <div class="carousel-item active">
            <img class="d-block w-100" src="assets\front\img\banners\1.jpg" alt="First slide">
         
         </div>
         <div class="carousel-item">
            <img class="d-block w-100" src="assets\front\img\banners\2.jpg" alt="Second slide">
           
         </div>
         <div class="carousel-item">
            <img class="d-block w-100" src="assets\front\img\banners\3.jpg" alt="Third slide">
            
         </div>
         <div class="carousel-caption d-none d-md-block main_content_pos">
               <div class="container">
                  <h1>Welcome to Spectra</h1>
                  <h2>Your Complete Sports Guide</h2>
                  <div class="d-flex align-items-center justify-content-center">
                     <i class="bx bxs-right-arrow-alt get-started-icon"></i>
                     <a href="#about" class="btn-get-started scrollto">Get Started</a>
                  </div>
               </div>
            </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
      </a>
   </div>
</section>
<!-- End Hero -->
<style>
   .price {
   border: 2px solid #eee;
   box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.2)
   }
   /* .price:hover {
   box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.2)
   } */
</style>
<main id="main">
   <section id="why-us" class="why-us">
      <div class="container  ">
         <div class="row ">
            <div class="col-xl-12 col-lg-12 d-flex ">
               <div class="icon-boxes d-flex flex-column justify-content-center  ">
                  <div class="row">
                     <div class="col-xl-3 d-flex align-items-stretch ">
                        <div class="bg-white box-shadow mt-4 mt-xl-0 price">
                           <div class="card-box pb--0">
                              <img src="assets\front\img\scout copy.png" class="rounded" width="80" height="80">
                              <br><br>
                              <h4>Scouting</h4>
                              </a>
                           </div>
                           <div class="card-box pt--0">
                              <p>Scouting solutions for Individual, Teams, Coaches and Sports Investors. Join us to explore great opportunity in sports</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 d-flex align-items-stretch">
                        <div class="bg-white box-shadow mt-4 mt-xl-0 price">
                           <div class="card-box pb--0">
                              <img src="assets\front\img\talent copy.png" class="rounded" width="80" height="80">
                              <br><br>
                              <h4>Talent Development</h4>
                              </a>
                           </div>
                           <div class="card-box pt--0">
                              <p>Enhance your talent and skills with exclusive personal guidance.</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 d-flex align-items-stretch">
                        <div class="bg-white box-shadow mt-4 mt-xl-0 price">
                           <div class="card-box pb--0">
                              <img src="assets\front\img\career copy.png" class="rounded" width="80" height="80">
                              <br><br>
                              <h4>Career Development</h4>
                              </a>
                           </div>
                           <div class="card-box pt--0">
                              <p>We here at spectra sports support career opportunities in different sports segments. For those who are looking for serious career opportunities in sports.</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-xl-3 d-flex align-items-stretch">
                        <div class="bg-white box-shadow mt-4 mt-xl-0 price">
                           <div class="card-box pb--0">
                              <img src="assets\front\img\fund copy.png" class="rounded" width="80" height="80">
                              <br><br>
                              <h4>Fund Raising</h4>
                              </a>
                           </div>
                           <div class="card-box pt--0">
                              <p>Financial aid and fund raising assistance for exceptional talents in sports.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="about" class="about section-bg1 about_spectra">
      <div class="container">
         <div class="row">
         <div class="col-md-6">
            <div class="video_box">
            <img src="assets/front/img/banners/scorebg.jpg" alt="">
             <div class="video-box ">
                <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true">
               </a>
                </div>
            </div>
            
          </div>
          <div class="col-md-6">
             <div>
             <h3>About Spectra</h3>
               <p>
                  Spectra Sports are the frontrunners in delivering 360° digital assistance for sports enthusiasts looking for a career or better and wider opportunity in sports.
               </p>
               <p>
                  Our diverse range of services includes enabling connections & visibility with scouts, introduction to talent hunts, career development & counselling, fund-raising and cloud-based data storage of all your performances.
               </p>
               <p>
                  As an ardent sports fanatic, we are passionate about driving our partners to their very best and help them build a career in sports.
               </p>
             </div>
          </div>
      
           
         </div>
      </div>
   </section>
   <section id="counts" class="counts counts_bg section-bg">
      <div class="container">
         <div class="row no-gutters">
            <div class="col-lg-2 col-md-6 d-md-flex align-items-md-stretch">
               <div class="count-box text-center ">
                  <?php
                     $query1 = "SELECT id FROM tbl_service_request WHERE service='1' ORDER BY id";
                     
                     $query_run = mysqli_query($conn, $query1);
                     
                     $row1 = mysqli_num_rows($query_run);
                     
                     ?>
                  <span data-toggle="counter-up" id="number1" class="number" data-number="<?php echo $row1; ?>">0</span>
                  <p class="margin0"><strong>Players</strong></p>
               </div>
            </div>
            <div class="col-lg-3 col-md-4 d-md-flex align-items-md-stretch">
               <div class="count-box text-center">
                  <?php
                     $query2 = "SELECT id FROM tbl_service_request WHERE service='2' ORDER BY id";
                     $query_run = mysqli_query($conn, $query2);
                     
                     $row2 = mysqli_num_rows($query_run);
                     ?>
                  <span data-toggle="counter-up" id="number2" class="number" data-number="<?php echo $row2; ?>">0</span>
                  <p class="margin0"><strong>Couches</strong></p>
               </div>
            </div>
            <div class="col-lg-2 col-md-4 d-md-flex align-items-md-stretch">
               <div class="count-box text-center">
                  <?php
                     $query3 = "SELECT id FROM tbl_service_request WHERE service='3' ORDER BY id";
                     $query_run = mysqli_query($conn, $query3);
                     
                     $row3 = mysqli_num_rows($query_run);
                     ?>
                  <span data-toggle="counter-up" id="number3" class="number" data-number="<?php echo $row3; ?>">0</span>
                  <p class="margin0"><strong>Mentors</strong></p>
               </div>
            </div>
            <div class="col-lg-3 col-md-4 d-md-flex align-items-md-stretch">
               <div class="count-box text-center">
                  <?php
                     $query4 = "SELECT id FROM tbl_service_request WHERE service='4' ORDER BY id";
                     $query_run = mysqli_query($conn, $query4);
                     
                     $row4 = mysqli_num_rows($query_run);
                     ?>
                  <span data-toggle="counter-up" id="number4" class="number" data-number="<?php echo $row4; ?>">0</span>
                  <p class="margin0"><strong>Universities</strong></p>
               </div>
            </div>
            <div class="col-lg-2 col-md-6 d-md-flex align-items-md-stretch">
               <div class="count-box text-center">
                  <?php
                     $query5 = "SELECT id FROM tbl_service_request WHERE service='5' ORDER BY id";
                     $query_run = mysqli_query($conn, $query5);
                     
                     $row5 = mysqli_num_rows($query_run);
                     ?>
                  <span data-toggle="counter-up" id="number5" class="number" data-number="<?php echo $row5; ?>">2</span>
                  <p class="margin0"><strong>Acadmies</strong></p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- ======= Banner Image Section ======= -->
   <section id="cta" class="cta">
      <div class="container">
         <div class="row aos-init aos-animate">
            <div class="col-lg-9 text-center text-lg-left">
               <h3>Call To Action</h3>
               <p>We are first in industry to provide digital assistance, Spectra sports offer extensive supports in all aspects of sports eg: Mentorship, Connecting with Sports Idols etc. </p>
            </div>
            <div class="col-lg-3 cta-btn-container text-center">
               <a class="cta-btn align-middle" href="#contact">Call To Action</a>
            </div>
         </div>
      </div>
   </section>
   <!-- End Banner Image Section -->
   <!-- ======= Values Section ======= -->
   <section>
      <div class="container">
         <div class="row align-items-center">
            <div class="col-xl-6 col-md-6">
               <div class="aims">
                  <img src="assets\front\img\aim.svg" alt="Pineapple">
               </div>
            </div>
            <div class="col-xl-6 col-md-6">
               <div class="aims">
                  <h1><b class="h1c">Our Aim</b></h1>
                  <p class="gg">
                     Spectra Sports aim to build a strong and powerful community that aligns with our values of constantly driving forward with passion, commitment and ambition. We strive to make meaningful and lasting connections amongst players, mentors, coaches, and other sports-related personnel to make it a go-to platform for complete sports assistance.
                  </p>
               </div>
            </div>
         </div>
         <div class="row align-items-center">
            <div class="col-xl-6 col-md-6">
               <div class="aims">
                  <h1><b class="h1c">Vision</b></h1>
                  <p class="gg">
                     Our vision is to build a global scale sports assistance platform by providing our very best services in the most practical and functional way to any dreamer from across the world.
                  </p>
               </div>
            </div>
            <div class="col-xl-6 col-md-6">
               <div class="aims">
                  <img src="assets\front\img\vision.svg " alt="Pineapple">
               </div>
            </div>
         </div>
         <div class="row align-items-center">
            <div class="col-xl-6 col-md-6">
               <div class="aims">
                  <img src="assets\front\img\mission.svg " alt="Pineapple">
               </div>
            </div>
            <div class="col-xl-6 col-md-6">
               <div class="aims">
                  <h1><b class="h1c">Mission</b></h1>
                  <p class="gg">
                     Our mission is to achieve hand in hand with our dreamers and quench the thirst of success and glory through our holistic step by step assistance.
                  </p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- End Values Section -->
   <!-- ======= About Section ======= -->

   <!-- End About Section -->
   <!-- ======= Events Section ======= -->
   <section class="team">
      <div class="container">
         <div class="section-title">
            <h2><a href="<?= base_url() ?>events">Upcoming Events</a></h2>
         </div>
         <style>
            .price {
            border: 2px solid #eee;
            }
            .price:hover {
            box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.9)
            }
         </style>
         <div class="row">
            <div class="col-xl-12">
               <div class="owl-carousel home-idols-carousel">
                  <?php foreach ($this->frontend_model->get_records('tbl_events', "status = '0' and event_status = 'approved' order by id desc limit 6") as $event) : ?>
                  <div class="item m--15">
                     <div class="card price dynamic_events">
                        <div>
                              <img class="card-img-top" src="<?= base_url() ?>uploads/<?= $event->event_image ?>" alt="<?= base_url() ?>uploads/<?= $event->event_image ?>" />
                           <div class="pos_date">
                              <span class="date"> <?= date('d', strtotime($event->event_date)) ?></span>
                              <div class="month_pos">
                              <span> <?= date('M', strtotime($event->event_date)) ?></span>
                              <span> <?= date('Y', strtotime($event->event_date)) ?></span>
                              </div>
                           
                           </div>
                        </div>
                      
                        <div class="card-body">
                           <h5 class="card-title"><?= $event->event_name ?></h5>
                          

                           <ul class="list-group list-group-flush blog_ul">
                              <li class="list-group-item"><i class="fa fa-icon fa-map-marker"></i> <span><?= $event->location ?></span> </li>
                              <li class="list-group-item">₹<?= $event->price ?>/- <?= $event->price_for ?></li>
                           </ul>
                           <p class="card-text font-13" style="text-overflow: ellipsis;"><?= $event->short_description ?></p>
                        </div>
                       
                        <div class="card-body d_flex">
                           <a href="<?= base_url() ?>event/<?= $event->id ?>/<?= $tis->slugify($event->event_name) ?>" class="card-link">Know More</a>
                           <a target="_blank" href="<?= $event->booking_link ?>" class="card-link btn btn-sm btn-secondary">Book Now</a>
                        </div>
                     </div>
                  </div>
                  <?php endforeach; ?>
               </div>
            </div>
            <!--<div class="content col-xl-4 d-flex flex-column justify-content-center">
               <img src="<?= base_url() ?>assets/front/img/counts-img.svg" class="img-fluid" alt="" />
               </div>-->
         </div>
      </div>
      </div>
   </section>
   <!-- End Events Section -->
   <!-- ======= Donation Section ======= -->
   <!-- <section id="donation" class="section-bg1">
      <div class="container">
         <div class="section-title">
            <h2>Support Them</h2>
            <div class="container">
               <p>
                  Support them with the financial aid that will uplift their career, Here are our athletes who will bring laurels to our country.
               </p>
            </div>
         </div>
         <div class="row">
        
            <div class="col-xl-12">
               <div class="owl-carousel support-them-carousel">
                  <?php foreach ($this->frontend_model->get_records("tbl_service_request", "service = '4' and request_status = 'approved' and status = '0' order by id desc limit 6") as $fr) : ?>
                  <div class="item m--15">
                     <div class="card fund-raiser-img-class">
                        <a href="<?= base_url() ?>fund-raiser/<?= $fr->id ?>/<?= $tis->slugify($fr->purpose) ?>">
                        <img class="card-img-top" src="<?= base_url() ?>uploads/service-requests/<?= $fr->raiser_image ?>" alt="<?= base_url() ?>uploads/service-requests/<?= $fr->raiser_image ?>" />
                        </a>
                        <div class="card-name">
                           <a href="<?= base_url() ?>fund-raiser/<?= $fr->id ?>/<?= $tis->slugify($fr->purpose) ?>">
                              <p>
                                 <?= $fr->purpose ?>
                              </p>
                           </a>
                        </div>
                        <div class="card-location">
                           <a href="<?= base_url() ?>fund-raiser/<?= $fr->id ?>/<?= $tis->slugify($fr->purpose) ?>">
                           <i class="fa fa-icon fa-map-marker"></i>
                           <?= $fr->city ?>, <?= $fr->state ?>
                           </a>
                           <a href="<?= base_url() ?>fund-raiser/<?= $fr->id ?>/<?= $tis->slugify($fr->purpose) ?>">
                           <?= $this->frontend_model->get_record("tbl_sports", "id=" . $fr->sport, "name") ?>
                           </a>
                        </div>
                       
                      
                        <?php
                           $percentage = ($fr->raised_amount / $fr->fund_required) * 100;
                           ?>
                        <div class="card-progress">
                           <div class="progress">
                              <div class="progress-bar" style="width: <?= $percentage ?>%;"></div>
                           </div>
                        </div>
                        <div class="card-stats row">
                           <div class="col-sm-4 text-center">
                              ₹<?= number_format($fr->raised_amount, 2) ?> <br />
                              <small>Raised</small>
                           </div>
                           <div class="col-sm-4 text-center">
                              ₹<?= number_format($fr->fund_required, 2) ?> <br />
                              <small>Target</small>
                           </div>
                           <div class="col-sm-4 text-center">
                              <span class="daysleft">
                              <?= sizeof($this->frontend_model->get_records("tbl_donors", "status = '0' and fund_raiser_id = '$fr->id' and is_paid = '1'")) ?>
                              </span> <br />
                              <small>Supporters</small>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php endforeach; ?>
               </div>
            </div>
         </div>
      </div>
   </section> -->
   <!-- End Donation Section -->
   <section id="team" class="team">
      <div class="container">
         <div class="section-title">
            <h2>Connect with your favourite Idols</h2>
            <p>First digital platform extensively for sports which will allow you to connect with your favourites. We provide up to date info about your favourite sports personalities and their performances. </p>
         </div>
         <div class="row  ">
            <!-- <div class="owl-carousel home-idols-carousel" id="rid"> -->
               <?php foreach ($this->frontend_model->get_custom_query("select * from tbl_idols") as $user) : ?>
               <div class="item p--17">
                  <div class="member">
                     <a href="<?= base_url() ?>profile/<?= $user->id ?>/<?= $tis->slugify($user->title) ?>">
                        <div class="member-img">
                           <img src="<?= base_url() ?><?= $user->image_url ?>" class="img-fluid" alt="<?= base_url() ?><?= $user->image_url ?>">
                           <!-- <img src="https://www.crictracker.com/wp-content/uploads/2021/03/Virat-Kohli-1-1.jpg"> -->
                           <div class="social">
                           <h4><?= ucfirst($user->title) ?> </h4>
                              <p>Demo</p>
                              <!-- <?= $user->description ?> -->
                           </div>
                        </div>
                        <div class="member-info">
                         
                           <span><?= $user->short_description ?></span>
                        </div>
                     </a>
                  </div>
               </div>
               <?php endforeach; ?>
            <!-- </div> -->
         </div>
      </div>
   </section>
   <?php if (0 == 1) : ?>
   <section id="venue" class="wow fadeInUp padding0">
      <div class="row no-gutters">
         <div class="col-lg-3 col-md-3">
            <div class="venue-gallery">
               <a href="assets/img/venue-gallery/1.jpg" class="venobox vbox-item" data-gall="venue-gallery">
               <img src="<?= base_url() ?>assets/front/img/testimonial-5.jpg" alt="" class="img-fluid" />
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-3">
            <div class="venue-gallery">
               <a href="assets/img/venue-gallery/1.jpg" class="venobox vbox-item" data-gall="venue-gallery">
               <img src="<?= base_url() ?>assets/front/img/testimonial-5.jpg" alt="" class="img-fluid" />
               </a>
            </div>
         </div>
         <div class="col-lg-6 venue-info">
            <div class="row justify-content-center">
               <div class="col-11 col-lg-8">
                  <h3>Connect with your favourite Idols</h3>
                  <p>
                     Iste nobis eum sapiente sunt enim dolores labore accusantium autem. Cumque beatae ipsam. Est quae sit qui voluptatem corporis velit. Qui maxime accusamus possimus. Consequatur sequi et ea suscipit enim
                     nesciunt quia velit.
                  </p>
               </div>
            </div>
         </div>
      </div>
      <div class="row no-gutters">
         <div class="col-lg-3 col-md-3">
            <div class="venue-gallery">
               <a href="assets/img/venue-gallery/1.jpg" class="venobox vbox-item" data-gall="venue-gallery">
               <!-- <img src="<?= base_url() ?>assets/front/img/testimonial-5.jpg" alt="" class="img-fluid" /> -->
               <img src="https://www.crictracker.com/wp-content/uploads/2021/03/Virat-Kohli-1-1.jpg">
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-3">
            <div class="venue-gallery">
               <a href="assets/img/venue-gallery/1.jpg" class="venobox vbox-item" data-gall="venue-gallery">
               <!-- <img src="<?= base_url() ?>assets/front/img/testimonial-5.jpg" alt="" class="img-fluid" /> -->
               <img src="https://www.crictracker.com/wp-content/uploads/2021/03/Virat-Kohli-1-1.jpg">
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-3">
            <div class="venue-gallery">
               <a href="assets/img/venue-gallery/1.jpg" class="venobox vbox-item" data-gall="venue-gallery">
               <!-- <img src="<?= base_url() ?>assets/front/img/testimonial-5.jpg" alt="" class="img-fluid" /> -->
               <img src="https://www.crictracker.com/wp-content/uploads/2021/03/Virat-Kohli-1-1.jpg">
               </a>
            </div>
         </div>
         <div class="col-lg-3 col-md-3">
            <div class="venue-gallery">
               <a href="assets/img/venue-gallery/1.jpg" class="venobox vbox-item" data-gall="venue-gallery">
               <!-- <img src="<?= base_url() ?>assets/front/img/testimonial-5.jpg" alt="" class="img-fluid" /> -->
               <img src="https://www.crictracker.com/wp-content/uploads/2021/03/Virat-Kohli-1-1.jpg">
               </a>
            </div>
         </div>
      </div>
   </section>
   <?php endif; ?>
   <style>
      .price {
      border: 2px solid #eee;
      }
      .price:hover {
      box-shadow: 0 5px 12px 0 rgba(0, 0, 0, 0.2)
      }
      .rid {
      border-radius: 15px;
      }
   </style>
   <!-- ======= Testimonials Section ======= -->
   <section id="testimonials" class="team section-bg1  ">
      <div class="container  ">
         <div class="section-title padding0">
            <h2>Happy Clients</h2>
         </div>
         <div class="row">
            <div class="owl-carousel testimonial-carousel owl-theme  ">
               <?php foreach ($this->frontend_model->get_custom_query("select * from tbl_feedback") as $user) : ?>
               <div class="item">
                  <div class="testimonial-item price rid">
                     <div class="testimonial_img">
                        <!-- <img src="https://www.crictracker.com/wp-content/uploads/2021/03/Virat-Kohli-1-1.jpg"> -->
                     <img src="<?= base_url() ?><?= $user->image_url ?>" class="testimonial-img" alt="" />
                   
                     </div>
                     <div class="testi_meta">
                     <h3><?= $user->title ?></h3>
                     <span>Co-Founder</span>
                     <p>  <?= $user->short_description ?></p>
                  
                     <p>
                        <!-- <?= $user->description ?> -->
                     </p>
                     </div>
                  
                    
                  </div>
               </div>
               <?php endforeach; ?>
            </div>
         </div>
      </div>
   </section>
   <!-- End Testimonials Section -->
   <!-- ======= Clients Section ======= -->
   <section id="clients" class="clients">
      <div class="container-fluid">
         <div class="owl-carousel clients-carousel">
            <img src="<?= base_url() ?>assets/front/img/clients/client-1.png" alt="" />
            <img src="<?= base_url() ?>assets/front/img/clients/client-2.png" alt="" />
            <img src="<?= base_url() ?>assets/front/img/clients/client-3.png" alt="" />
            <img src="<?= base_url() ?>assets/front/img/clients/client-4.png" alt="" />
            <img src="<?= base_url() ?>assets/front/img/clients/client-5.png" alt="" />
            <img src="<?= base_url() ?>assets/front/img/clients/client-6.png" alt="" />
            <img src="<?= base_url() ?>assets/front/img/clients/client-7.png" alt="" />
            <img src="<?= base_url() ?>assets/front/img/clients/client-8.png" alt="" />
         </div>
      </div>
   </section>
   <!-- End Clients Section -->
   <hr>
   <!-- ======= Contact Section ======= -->
   <section id="contact" class="contact">
      <div class="container">
         <div class="section-title padding0">
            <h2>Contact</h2>
            <!-- <p>
               Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste
               officiis commodi quidem hic quas.
            </p> -->
         </div>
         <div class="row justify-content-center">
            <div class="col-xl-3 col-lg-4 mt-4">
               <div class="info-box">
                  <i class="bx bx-map"></i>
                  <h3>Our Address</h3>
                  <p>No.18, Sri Sai Nivas, Ranganathan Street, Radhanagar, Chromepet,Chennai-44</p>
               </div>
            </div>
            <div class="col-xl-3 col-lg-4 mt-4">
               <div class="info-box">
                  <i class="bx bx-envelope"></i>
                  <h3>Email Us</h3>
                  <p>
                     admin@spectrasports.in
                     <br><br><br><br>
                  </p>
               </div>
            </div>
            <div class="col-xl-3 col-lg-4 mt-4">
               <div class="info-box">
                  <i class="bx bx-phone-call"></i>
                  <h3>Call Us</h3>
                  <p>
                     +91-9840802580
                     <br><br><br><br>
                  </p>
               </div>
            </div>
         </div>
         <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-12 mt-4">
               <form action="forms/contact.php" method="post" role="form" class="php-email-form">
                  <div class="form-row">
                     <div class="col-md-6 form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        <div class="validate"></div>
                     </div>
                     <div class="col-md-6 form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                        <div class="validate"></div>
                     </div>
                  </div>
                  <div class="form-group">
                     <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                     <div class="validate"></div>
                  </div>
                  <div class="form-group">
                     <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                     <div class="validate"></div>
                  </div>
                  <div class="mb-3">
                     <div class="loading">Loading</div>
                     <div class="error-message"></div>
                     <div class="sent-message">Your message has been sent. Thank you!</div>
                  </div>
                  <div class="text-center"><button type="submit">Send Message</button></div>
               </form>
            </div>
         </div>
      </div>
   </section>
   <!-- End Contact Section -->
</main>
<!-- End #main -->