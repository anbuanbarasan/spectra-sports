<?php
$selectedId = $_GET['id'];
$name = $_GET['name'];
require_once(APPPATH.'config/database_config.php');
$query = "SELECT * FROM tbl_membership WHERE status='1'";
$query_run = mysqli_query($conn, $query);
?>
<style>
    .single-price {
        text-align: center;
        background: #262626;
        transition: .7s;
        margin-top: 20px;
    }

    .single-price h3 {
        font-size: 25px;
        color: #000;
        font-weight: 600;
        text-align: center;
        margin: 0;
        margin-top: -80px;
        font-family: poppins;
        color: #fff;
    }

    .single-price h4 {
        font-size: 48px;
        font-weight: 500;
        color: #fff;
    }

    .single-price h4 span.sup {
        vertical-align: text-top;
        font-size: 25px;
    }

    .deal-top {
        position: relative;
        background: #f22c4d;
        font-size: 16px;
        text-transform: uppercase;
        padding: 136px 24px 0;
    }

    .deal-top::after {
        content: "";
        position: absolute;
        left: 0;
        bottom: -50px;
        width: 0;
        height: 0;
        border-top: 50px solid #f22c4d;
        border-left: 127px solid transparent;
        border-right: 127px solid transparent;
    }

    .deal-bottom {
        padding: 56px 16px 0;
    }

    .deal-bottom ul {
        margin: 0;
        padding: 0;
    }

    .deal-bottom ul li {
        font-size: 16px;
        color: #fff;
        font-weight: 300;
        margin-top: 16px;
        border-top: 1px solid #E4E4E4;
        padding-top: 16px;
        list-style: none;
    }

    .btn-area a {
        display: inline-block;
        font-size: 18px;
        color: #fff;
        background: #f22c4d;
        padding: 8px 64px;
        margin-top: 32px;
        border-radius: 4px;
        margin-bottom: 40px;
        text-transform: uppercase;
        font-weight: bold;
        text-decoration: none;
    }


    .single-price:hover {
        background: #f22c4d;
    }

    .single-price:hover .deal-top {
        background: #262626;
    }

    .single-price:hover .deal-top:after {
        border-top: 50px solid #262626;
    }

    .single-price:hover .btn-area a {
        background: #262626;
    }





    /* ignore the code below */


    .link-area {
        position: fixed;
        bottom: 20px;
        left: 20px;
        padding: 15px;
        border-radius: 40px;
        background: tomato;
    }

    .link-area a {
        text-decoration: none;
        color: #fff;
        font-size: 25px;
    }
</style>

<!-- Contenedor -->


<!-- Titulo -->


<div class="pricing-area">

    <div class="container">
        <div class="row">
                <?php
                if (mysqli_num_rows($query_run) > 0) {
                    while ($row = mysqli_fetch_assoc($query_run)) {
                        $id=$row['id'];
                        $price=$row['price'];
                ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
               
                        <div class="single-price">
                            <div class="deal-top">
                                <h3><?php echo $row['type']; ?></h3>
                                <h4> <?php echo $price; ?> <span class="sup">₹</span> </h4>
                            </div>
                            <div class="deal-bottom">
                                <ul class="deal-item">
                                    <li><?php echo $row['line1']; ?></li>
                                    <li><?php echo $row['line2']; ?></li>
                                    <li><?php echo $row['line3']; ?></li>
                                    <li><?php echo $row['line4']; ?></li>
                                    <li><?php echo $row['line5']; ?></li>
                                </ul>
                                <div class="btn-area">
                                <a href="<?= base_url() ?>apply-for-services/<?= $selectedId ?>/<?= $name ?>?membershipId=<?= $id ?>&price=<?= $price ?>" class="pricing-action">Buy</a>
                                </div>
                            </div>
                        </div>

               

            </div>
            <?php
                    }
                }
                ?>

        </div>
    </div>



</div>







<br>
<br>
<br>