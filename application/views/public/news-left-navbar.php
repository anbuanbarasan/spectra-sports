<!--start right column-->
<aside class="col-md-3">
	<div class="sticky">

		<!-- latest post -->
		<aside class="widget">
			<!--Block title-->
			<div class="block-title-4">
				<h4 class="h5 title-arrow">
					<span>Cricket</span>
				</h4>
			</div>
			<div class="small-post">
				<?php $i = 1; ?>
				<?php foreach($recent_fixtures as $recent_fixture): ?>
					<?php if($i < 5): ?>
						<article class="card card-full hover-a mb-4">
							<div class="row">
								<div class="col-12 col-md-12">
									<div class="card-body pt-2">
										<div class="row">
											<div class="col-5 col-md-5">
												<img src="<?=$tis->get_team_by_id($recent_fixture['localteam_id'])['image_path']?>" width="25">
												<?=$tis->get_team_by_id($recent_fixture['localteam_id'])['name']?>
											</div>
											<div class="col-2 col-md-2">
												vs
											</div>
											<div class="col-5 col-md-5">
												<img src="<?=$tis->get_team_by_id($recent_fixture['visitorteam_id'])['image_path']?>" width="25">
												<?=$tis->get_team_by_id($recent_fixture['visitorteam_id'])['name']?>
											</div>
											<div class="col-12 col-md-12">
												<div class="card-text small text-muted text-center">
													<time>
														<?=$recent_fixture['type']?> : <?=($recent_fixture['live'] == false)?"<b class='text-danger'>Finished</b>":"<b class='text-success'>Live</b>"?>
													</time>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</article>
					<?php else: ?>
						<?php break; ?>
					<?php endif; ?>
					<?php $i++; ?>
				<?php endforeach; ?>
			</div>
			<!--end post small-->
			<div class="gap-0"></div>
		</aside>
		<!-- end latest post -->

	</div>
</aside>
<!--end right column-->