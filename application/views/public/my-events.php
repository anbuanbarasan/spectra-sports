
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				<?php  	 ?>
			</div>
			<div class="col-md-12">
				<div class="mt--30 mb--30">
					<div class="card">
						<div class="card-header">
							<h5>Post a event</h5>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<table class="table table-bordered">
										<thead>
											<tr>
												<td>Date</td>
												<td>Event Name</td>
												<td>Status</td>
												<td></td>
											</tr>
										</thead>
										<tbody>
											<?php foreach($records as $record): ?>
												<tr>
													<td><?=date('d M, Y', strtotime($record->event_date));?></td>
													<td><?=$record->event_name;?></td>
													<td>
														<?=ucfirst($record->event_status)?>
													</td>
													<td>
														<a href="<?=base_url()?>edit-event/<?=$record->id?>/<?=$tis->slugify($record->event_name)?>" class="btn btn-sm btn-warning">Edit</a>
													</td>
												</tr>
											<?php endforeach; ?>
										</tbody>
									</table>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>