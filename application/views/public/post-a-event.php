
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3">
				 <?  ?> 
			</div>
			<div class="col-md-9">
				<div class="mt--30 mb--30">
					<form class="new-event" enctype="multipart/form-data">
						<div class="card">
							<div class="card-header">
								<h5>Post a event</h5>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-6"> 
										<p class="form-row form-row-wide">
											<label for="event_name">Event Name <span class="required">*</span></label>
											<input type="text" class="form-control required" name="event_name" id="event_name" value="" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="short_description">Short Description <span class="required">*</span></label>
											<textarea class="form-control required" type="text" name="short_description" id="short_description" required="" autocomplete="off"></textarea>
										</p>
										<div class="row">
											<div class="col-md-6">
												<p class="form-row form-row-wide">
													<label for="price">Price <span class="required">*</span></label>
													<input class="form-control required" name="price" id="price" type="number" required="" autocomplete="off">
												</p>
											</div>
											<div class="col-md-6">
												<p class="form-row form-row-wide">
													<label for="price_for">For <span class="required">*</span></label>
													<select class="form-control" name="price_for" id="price_for">
														<option>per ticket</option>
														<option>per team</option>
														<option>onwards</option>
													</select>
												</p>
											</div>
										</div>
										<p class="form-row form-row-wide">
											<label for="booking_link">Booking Link <span class="required">*</span></label>
											<input type="url" class="form-control required" name="booking_link" id="booking_link" value="" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label for="sport">Choose Sport <span class="required">*</span></label>
											<select class="form-control" name="sport" id="sport">	
												<?php foreach($this->frontend_model->get_records('tbl_sports', "status = '0'") as $sport): ?>
												<option value="<?=$sport->id?>"><?=$sport->name?></option>
												<?php endforeach; ?>
											</select>
										</p>	
									</div>
									<div class="col-md-6">
										<p class="form-row form-row-wide">
											<label for="event_date">Event Date <span class="required">*</span></label>
											<input type="date" class="form-control required" name="event_date" id="event_date" required="" autocomplete="off" value="<?=date('Y-m-d')?>">
										</p>
										<p class="form-row form-row-wide">
											<label for="location">Location / Venue<span class="required">*</span></label>
											<input type="text" class="form-control required" name="location" id="location" value="" required="" autocomplete="off">
										</p>
										<p class="form-row form-row-wide">
											<label>Event Poster <span class="required">*</span></label>
											<div class="input-images-1"></div>
										</p>
									</div>
									<div class="col-md-12">
										<p class="form-row form-row-wide">
											<label for="editor">Description <span class="required">*</span></label><br>
											<textarea class="form-control required" name="description" id="editor" type="text" required="" autocomplete="off"></textarea>
										</p>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<p class="form-row form-row-wide">
									<button type="submit" class="button pull-right">Submit</button>
								</p>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<link href="<?=base_url()?>assets/front/css/img-uploader.css" rel="stylesheet" />
	<script src="<?=base_url()?>assets/front/js/img-uploader.js"></script>
	<script src="https://ckeditor.com/latest/ckeditor.js"></script>
	<script>
		CKEDITOR.replace('editor', {
			removeButtons: 'Source'
		});
		$(function () {
			$('.input-images-1').imageUploader({
				imagesInputName: 'event_image',
				extensions: ['.jpg','.jpeg','.png','.gif','.JPG','.JPEG','.PNG','.GIF'],
				mimes: ['image/jpeg','image/jpg','image/png','image/gif'],
			});
			$('input[type=file]').attr('name', 'event_image');
			$('input[type=file]').removeAttr('multiple');
		});
	</script>