<?php
// $selectedId = $_GET['id'];
// $name = $_GET['name'];
require_once(APPPATH.'config/database_config.php');

?>

<style>
    /*--thank you pop starts here--*/
    .thank-you-pop {
        width: 100%;
        padding: 20px;
        text-align: center;
    }

    .thank-you-pop img {
        width: 76px;
        height: auto;
        margin: 0 auto;
        display: block;
        margin-bottom: 25px;
    }

    .thank-you-pop h1 {
        font-size: 42px;
        margin-bottom: 25px;
        color: #5C5C5C;
    }

    .thank-you-pop p {
        font-size: 20px;
        margin-bottom: 27px;
        color: #5C5C5C;
    }

    .thank-you-pop h3.cupon-pop {
        font-size: 25px;
        margin-bottom: 40px;
        color: #222;
        display: inline-block;
        text-align: center;
        padding: 10px 20px;
        border: 2px dashed #222;
        clear: both;
        font-weight: normal;
    }

    .thank-you-pop h3.cupon-pop span {
        color: #03A9F4;
    }

    .thank-you-pop a {
        display: inline-block;
        margin: 0 auto;
        padding: 9px 20px;
        color: #fff;
        text-transform: uppercase;
        font-size: 14px;
        background-color: #8BC34A;
        border-radius: 17px;
    }

    .thank-you-pop a i {
        margin-right: 5px;
        color: #fff;
    }

    #ignismyModal .modal-header {
        border: 0px;
    }

    /*--thank you pop ends here--*/
</style>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="mt--30 mb--30 align-items-stretch">
                <div class="bg-white box-shadow mt-4 mt-xl-0">
                    <div class="card-box pb--0">
                        <img src="<?= base_url() ?>uploads/<?= $service->image ?>" width="75">
                        <br>
                        <br>
                        <h4><?= $service->name ?></h4>
                    </div>
                    <div class="card-box pt--0 min-height-150px">
                        <p><?= $service->short_description ?></p>
                    </div>
                    <div class="card-box pt--0">
                        <a class="card-link btn btn-sm btn-secondary" href="<?= base_url() ?>services/<?= $service->id ?>/<?= $tis->slugify($service->name) ?>">Know more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="mt--30 mb--30">
            <form method="post" class="service-new-request" enctype="multipart/form-data">
                <input type="hidden" value="<?php echo $_GET['price'];?>" name="raised_amount" id="price"/>
                <input type="hidden" value="<?php echo $_GET['membershipId'];?>" name="membership_id" id="membershipId"/>
                    <div class="card">
                        <div class="card-header">
                            <h5><?= $service->name ?></h5>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="name">Name <span class="required">*</span></label>
                                        <input type="text" class="form-control required" name="name" id="name" value="<?= $login_name ?>" required="" autocomplete="off">
                                        <input type="hidden" name="service" value="<?= $service->id ?>">
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="date_of_birth">Date of birth <span class="required">*</span></label>
                                        <input class="form-control required" type="date" value="<?= $user_details->date_of_birth ?>" name="date_of_birth" id="date_of_birth" required="" autocomplete="off">
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="gender">Gender <span class="required">*</span></label>
                                        <select class="form-control required" name="gender" id="gender" type="number" required="" autocomplete="off">
                                            <option <?= ($user_details->gender == "male") ? "selected" : "" ?> value="male">Male</option>
                                            <option <?= ($user_details->gender == "female") ? "selected" : "" ?> value="female">Female</option>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="sport">Choose Sport <span class="required">*</span></label>
                                        <select class="form-control" name="sport" id="sport">
                                            <?php foreach ($this->frontend_model->get_records('tbl_sports', "status = '0'") as $sport) : ?>
                                                <option value="<?= $sport->id ?>"><?= $sport->name ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="address">Address <span class="required">*</span></label>
                                        <input type="text" class="form-control required" value="<?= $user_details->address ?>" name="address" id="address" required="" autocomplete="off">
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="city">City <span class="required">*</span></label>
                                        <input type="text" class="form-control required" value="<?= $user_details->city ?>" name="city" id="city" required="" autocomplete="off">
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="state">State <span class="required">*</span></label>
                                        <input type="text" class="form-control required" value="<?= $user_details->state ?>" name="state" id="state" required="" autocomplete="off">
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="pincode">Pincode <span class="required">*</span></label>
                                        <input type="text" class="form-control required" value="<?= $user_details->pincode ?>" name="pincode" id="pincode" required="" autocomplete="off">
                                    </p>
                                </div>

                                <?php if ($this->frontend_model->get_record("tbl_general_users", "id = '$_SESSION[login_id]'", "service") != '1') : ?>
                                    <div class="col-md-6">
                                        <p class="form-row form-row-wide">
                                            <label for="designation">Designation <span class="required">*</span></label>
                                            <input type="text" class="form-control required" name="designation" id="designation" required="" autocomplete="off">
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="form-row form-row-wide">
                                            <label for="name_of_facility">Name of the facility <span class="required">*</span></label>
                                            <input type="text" class="form-control required" name="name_of_facility" id="name_of_facility" required="" autocomplete="off">
                                        </p>
                                    </div>
                                <?php endif; ?>

                                <?php if ($service->id == '1') : ?>
                                    <div class="col-md-6">
                                        <p class="form-row form-row-wide">
                                            <label for="level_of_participation"> Level of participation <span class="required">*</span></label>
                                            <input type="text" class="form-control required" name="level_of_participation" id="level_of_participation" required="" autocomplete="off">
                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if ($service->id == '4') : ?>
                                    <div class="col-md-6">
                                        <p class="form-row form-row-wide">
                                            <label for="purpose">Purpose in short <span class="required">*</span></label>
                                            <input type="text" class="form-control required" name="purpose" id="purpose" required="" autocomplete="off" placeholder="Raising funds for">
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <p class="form-row form-row-wide">
                                            <label for="fund_required">Fund required <span class="required">*</span></label>
                                            <input type="text" class="form-control required" name="fund_required" id="fund_required" required="" autocomplete="off" placeholder="How much do you want to raise?">
                                        </p>
                                    </div>
                                    <div class="col-md-12">
                                        <p class="form-row form-row-wide">
                                            <label for="described_purpose"> Describe purpose <span class="required">*</span></label>
                                            <textarea type="text" class="form-control required" name="described_purpose" id="described_purpose" required="" autocomplete="off" placeholder="Describe your fundraiser purpose"></textarea>
                                        </p>
                                    </div>
                                <?php endif; ?>
                                <?php if ($service->id == '3') : ?>
                                    <div class="col-md-6">
                                        <p class="form-row form-row-wide">
                                            <label for="career_option"> Career option <span class="required">*</span></label>
                                            <select class="form-control required" name="career_option" id="career_option" required="" autocomplete="off">
                                                <option>Job</option>
                                                <option>Higher Studies</option>
                                                <option>Staffing</option>
                                            </select>
                                        </p>
                                    </div>
                                <?php endif; ?>
                                <div class="col-md-12">
                                    <p class="form-row form-row-wide">
                                        <label for="specific_requirements"> Specific requirement <span class="required">*</span></label>
                                        <textarea type="text" class="form-control required" name="specific_requirements" id="specific_requirements" required="" autocomplete="off" placeholder="Describe your specific requirements"></textarea>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <p class="form-row form-row-wide">
                                        <label for="proof_upload"> Proof upload <span class="required">*</span></label>
                                        <input type="file" class="form-control" name="proof_upload" id="proof_upload" accept=".jpeg,.jpg,.png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" required>
                                    </p>
                                </div>
                                <?php if ($service->id == '3') : ?>
                                    <div class="col-md-6">
                                        <p class="form-row form-row-wide">
                                            <label for="proof_upload"> Resume upload <span class="required">*</span></label>
                                            <input type="file" class="form-control" name="resume_upload" id="resume_upload" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" required>
                                        </p>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="card-footer">
                            <p class="form-row form-row-wide"> 
                            <button type="submit" class="card-link btn btn-sm btn-secondary" >Submit</button>
                            </p>
                        </div>
                        <div class="container">
                            <div class="row">

                                <div class="modal fade" id="ignismyModal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                                            </div>

                                            <div class="modal-body">

                                                <div class="thank-you-pop">
                                                    <img src="http://goactionstations.co.uk/wp-content/uploads/2017/03/Green-Round-Tick.png" alt="">
                                                    <h1>Thank You!</h1>
                                                    <p>Your submission is received and we will contact you soon</p>
                                                    <h3 class="cupon-pop">Your Id: <span>12345</span></h3>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://js.instamojo.com/v1/checkout.js"></script>
<script>
	function payment(url) {
		Instamojo.open(url);
	}
</script> 