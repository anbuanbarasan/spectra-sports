<main id="main">

    <section id="why-us" class="why-us">
      <div class="container">

        
		<style>
			 .price{
			 border: 1px solid #eee;
			 }
		         .price:hover {
               box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
              }
          </style>
		
		<div class="row ">
			<div class="col-xl-4 ">
				<div class="card price">
					<div class="card-body np_overlow">
						 <h5 class="card-title"><b><?=$record->event_name?></b></h5> 
					</div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item"><?=date('d M,  Y', strtotime($record->event_date))?></li> 
						<li class="list-group-item">₹ <?=$record->price?>/- <?=$record->price_for?></li>
						<li class="list-group-item"><?=$record->location?></li>
					</ul>
					<style>
                        div.c{
                         text-align: right;
						}
					</style>
					<div class="card-body c">
						<a target="_blank" href="<?=$record->booking_link?>" class="card-link btn btn-sm btn-secondary">Book Now</a>
					</div>
				</div>
			</div>
			<div class="col-xl-8 align-items-stretch">
				<div class="banner_img">
				<img class="w-100" src="https://creativelayers.net/themes/edumy-html/images/blog/12.jpg">
				<!-- <img class="w-100" src="<?=base_url()?>uploads/<?=$record->event_image?>"> -->
				</div>
				<div class="price">
					<div class="card-body">
						<?=html_entity_decode($record->description)?> 
					</div>
				</div>
			</div>
        </div>

      </div>
    </section>

</main>
