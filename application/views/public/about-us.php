<html lang="en">
 

<style>
 
 .price {
         /* border: 2px solid #eee; */
         /* box-shadow: 0 8px 12px 0 rgba(0, 0, 0, 0.2) */
     }


.inner-container{

    width: 100%;
    /* opacity: 0.5; */
	border-radius: 25px;
    /* padding: 55px; */
}
.about_us{
    padding:50px 0
}
.inner-container h1{
    margin-bottom: 30px;
    color: #000;
    font-size: 30px;
    font-weight: 900;
}

.text{
	/* text-indent: 50px; */
    font-size: 14px;
    color: #000;
    line-height: 30px;
    /* text-align: justify; */
    margin-bottom: 10px;
}

.skills{
	color: #ffffff;
    display: flex;
    justify-content: space-between;
    font-weight: 700;
    font-size: 13px;
}

@media screen and (max-width:1200px){
    .inner-container{
        padding: 80px;
    }
}

@media screen and (max-width:1000px){
    .about-section{
        background-size: 100%;
        padding: 100px 40px;
    }
    .inner-container{
        width: 100%;

    }
}

@media screen and (max-width:600px){
    .about-section{
        padding: 0;
    }
    .inner-container{
        padding: 60px;
    }
}
</style>

<body>
   
    <div class="about_us">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                        <div class="inner-container price">
                                    <h1>About Us</h1>
                                    <p class="text">
                                    Spectra Sports is one of a kind sports management service company which intend to bring all sports requirements in one place digitally. We provide services that would be highly needed for any sports athlete under one platform. Our aim is to provide any and every service that a sports person would require to perform at desired level. We also create a sports ecosystem where each service will be closely related to one another and every sports person needed.  
                        </p>
                        <p class="text">
                        Spectra sports provide an individual user profile where one can connect with other athlete of same sport or another sport. Individual user can also connect with their mentors, coaches and professionals and follow their activities closely. By creating user profile for individuals, help them to store their personal information and records. We intend to have create sports portfolio for individuals based on the request of individual interest. 
                                    </p>
                    
                
                        </div>
                </div>
                <div class="col-md-6">
                    <img src="assets\front\img\about-bg.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
    <section id="why-us" class="why-us add-top">
      <div class="container">
          <div class="section-title">
            <h2>Our Team</h2>
         </div>
         <div class="row ">
            <div class="col-xl-12 col-lg-12 d-flex ">
               <div class="icon-boxes d-flex flex-column justify-content-center  ">
                  <div class="row">
                  <?php foreach ($this->frontend_model->get_custom_query("select * from tbl_teams") as $service) : ?>
                    <div class="col-xl-3 ">
                        <div class="team-thumb">
                            <figure><img src="<?= base_url() ?><?= $service->image ?>" alt="thumb"></figure>
                        </div>
                        <div class="team_desc">
                            <h4><?= $service->name ?></h4>
                            <p><?= $service->designation ?></p>
                        </div>
                     </div>

                    <?php endforeach; ?>
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</body>
</html>

