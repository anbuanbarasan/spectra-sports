	
		<div id="register" class="modal fade" role="dialog">
		  <div class="modal-dialog">
			<div class="modal-content">
			  <div class="modal-body">
				<button data-dismiss="modal" class="close">×</button>
				<div class="col2-set" id="customer_login2">
					<div class="col-lg-12">
						<div class="form-design">
							<h4>Register</h4>
							<hr>
							<form method="post" class="register form_register">
								<p class="form-row form-row-wide">
									<label for="first_name">First Name <span class="required">*</span></label>
									<input type="text" class="form-control" name="first_name" id="first_name" required/>
								</p>
								<p class="form-row form-row-wide">
									<label for="last_name">Last Name <span class="required">*</span></label>
									<input type="text" class="form-control" name="last_name" id="last_name" required/>
								</p>
								<p class="form-row form-row-wide">
									<label for="last_name">Email <span class="required">*</span></label>
									<input type="text" class="form-control" name="last_name" id="last_name" required/>
								</p>
								<p class="form-row form-row-wide">
									<label for="phone_number">Mobile Number <span class="required">*</span></label>
									<input type="text" class="form-control" name="phone_number" id="phone_number" required/>
								</p>
								<p class="form-row form-row-wide">
									<label for="service">Choose Role <span class="required">*</span></label>
									<select class="form-control" name="service" id="service">	
										<?php foreach($this->frontend_model->get_records('tbl_services', "status = '0'") as $service): ?>
										<option value="<?=$service->id?>"><?=$service->name?></option>
										<?php endforeach; ?>
									</select>
								</p>
								<div></div>
								<p class="form-row form-row-wide">
									<label for="r_password">Password <span class="required">*</span></label>
									<input type="password" class="form-control" name="password" id="r_password" required/>
								</p>
								<p class="form-row form-row-wide">
									<label for="confirm_password">Confirm Password <span class="required">*</span></label>
									<input type="password" class="form-control" name="confirm_password" id="confirm_password" required/>
								</p>
								
								<p class="form-row form-row-wide">
									<label for="terms">
										<input type="checkbox" id="terms" required name="terms" class="input-checkbox">
										I’ve read and accept the 
										<a target="_blank" href="<?=base_url()?>terms-conditions" class="text-underline">terms &amp; conditions</a> 
										<span class="required">*</span>
									</label>
								</p>
								<p class="form-row form-row-wide">
									<button type="submit" class="button pull-right btn btn-danger">Register</button>
								</p>
							</form>	
							<hr>
							<small><a href="<?=base_url()?>login">Have an account? Login here.</a></small><br>
									
						</div>
					</div>
				</div>
			  </div>
			</div>
		  </div>  
		</div>
		