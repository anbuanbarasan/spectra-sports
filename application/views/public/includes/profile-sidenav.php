<nav id="sidebar" class="sidebar-wrapper">
	<div class="sidebar-content">
	  <div class="sidebar-menu">
		<ul>
		  <li class="header-menu">
			<span><?=$login_name?></span>
		  </li>
		  <li>
			<a href="<?=base_url()?>home">
			  <i class="fa fa-user"></i>
			  <span>My Home</span>
			</a>
		  </li>
		  <li>
			<a href="<?=base_url()?>my-profile">
			  <i class="fa fa-user"></i>
			  <span>My Profile</span>
			</a>
		  </li>
		  <li class="sidebar-dropdown">
			<a href="javascript:void(0);">
			  <i class="fa fa-book"></i>
			  <span>Events</span>			  
			</a>
			<div class="sidebar-submenu">
			  <ul>
				<li>
				  <a href="<?=base_url()?>post-a-event">Post a event</a>
				</li>
				<li>
				  <a href="<?=base_url()?>my-events">My Events</a>
				</li>
			  </ul>
			</div>
		  </li>
		  <li>
			<a href="<?=base_url()?>my-account">
			  <i class="fa fa-cog"></i>
			  <span>Account Settings</span>
			</a>
		  </li>
		</ul>
	  </div>
	  <!-- sidebar-menu  -->
	</div>
	<!-- sidebar-content  -->
</nav>

<?php if(0 == 1): ?>
	<span class="badge badge-pill badge-warning">New</span>
	<span class="badge badge-pill badge-primary">Beta</span>
	<span class="badge badge-pill badge-danger">3</span>
<?php endif; ?>