<meta name="viewport" content="width=device-width, initial-scale=1.0">
 

<main id="main">

  <section id="why-us" class="why-us">
    <div class="container">

      <div class="row">
        <div class="col-xl-12 col-lg-12 d-flex">
          <div class="icon-boxes d-flex flex-column justify-content-center">
            <div class="row">

              <?php foreach ($this->frontend_model->get_records("tbl_all_services", "status = 0 AND type='normal'") as $service) : ?>

                <div class="col-xl-3 d-flex align-items-stretch">
                  <div class="bg-white box-shadow mt-4 mt-xl-0">
                    <div class="card-box pb--0">
                      <img src="<?= base_url() ?>uploads/<?= $service->image ?>" width="75">
                      <br>
                      <br>
                      <a href="<?= base_url() ?>services/<?= $service->id ?>/<?= $tis->slugify($service->name) ?>">
                        <h4>
                          <?= $service->name ?>
                        </h4>
                      </a>
                    </div>
                    <div class="card-box pt--0 min-height-150px">
                      <p><?= $service->short_description ?></p>
                    </div>
                    <div class="card-box pt--0">
                      <?php if (isset($_SESSION['is_login'])) : ?>
                       
                          <a class="card-link btn btn-sm btn-secondary" href="<?=base_url()?>paylist?id=<?= $service->id ?>&name=<?=$service->name?>"> Apply</a>
 

                      <?php else : ?>
                        <button data-target="#login" data-toggle="modal" type="submit" class="button">Apply</button>

                      <?php endif; ?>
                    </div>
                  </div>
                </div>

              <?php endforeach; ?>



            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

         <section id="why-us" class="why-us">
    <div class="container">
    <div class="section-title">
            <h2>Additional Service</h2>
         </div>
      <div class="row">
        <div class="col-xl-12 col-lg-12 d-flex">
          <div class="icon-boxes d-flex flex-column justify-content-center">
            <div class="row">

              <?php foreach ($this->frontend_model->get_records("tbl_all_services", "status = 0 AND type='additional'") as $service) : ?>

                <div class="col-xl-3 d-flex align-items-stretch">
                  <div class="bg-white box-shadow mt-4 mt-xl-0">
                    <div class="card-box pb--0">
                      <img src="<?= base_url() ?>uploads/<?= $service->image ?>" width="75">
                      <br>
                      <br>
                      <a href="<?= base_url() ?>services/<?= $service->id ?>/<?= $tis->slugify($service->name) ?>">
                        <h4>
                          <?= $service->name ?>
                        </h4>
                      </a>
                    </div>
                    <div class="card-box pt--0 min-height-150px">
                      <p><?= $service->short_description ?></p>
                    </div>
                    <div class="card-box pt--0">
                      <?php if (isset($_SESSION['is_login'])) : ?>
                       
                          <a class="card-link btn btn-sm btn-secondary" href="<?=base_url()?>paylist?id=<?= $service->id ?>&name=<?=$service->name?>"> Apply</a>
 

                      <?php else : ?>
                        <button data-target="#login" data-toggle="modal" type="submit" class="button">Apply</button>

                      <?php endif; ?>
                    </div>
                  </div>
                </div>

              <?php endforeach; ?>



            </div>
          </div>
        </div>
      </div>

    </div>
  </section>
</main>
<!-- onclick="openSearch(<?= $service->id ?>)"  -->