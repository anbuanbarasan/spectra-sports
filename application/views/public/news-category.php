<main id="content" class="mt--10">
    <div class="container">
        <div class="row mt--10">
            <!-- top section -->
            <div class="col-12 mt-05 mt--10">
                <!-- Big grid slider 1 -->
                <div class="row featured-1 mb-5 mt--10">

                    <!-- start left column -->
                    <div class="col-md-8">
                        <!-- block start -->
                        <div class="block-area">
                            <!-- block title -->
                            <div class="block-title-6">
                                <h4 class="h5 border-primary">
                                    <span class="bg-primary text-white">
										<?=$news_category->name?>
									</span>
                                </h4>
                            </div>
							<!-- block content -->
							<div class="border-bottom-last-0 first-pt-0">
								<?php 
									foreach($news_details as $news):
									$link = base_url(). "post/" . $news->id . "/" . $tis->slugify($news->title);
									if($news->redirect_link != "")
									{
										$link = $news->redirect_link;
									}
								?>
									<article class="card card-full hover-a py-4">
										<div class="row">
											<div class="col-sm-6 col-md-12 col-lg-6">
												<div class="ratio_360-202 image-wrapper">
													<a href="<?=$link?>">
														<img class="img-fluid lazy" src="<?=base_url()?>assets/front/news/assets/img/assets/lazy-empty.png" data-src="<?=base_url()?>uploads/news/<?=$news->image?>" alt="<?=base_url()?>uploads/news/<?=$news->image?>">
													</a>
												</div>
											</div>
											<div class="col-sm-6 col-md-12 col-lg-6">
												<div class="card-body pt-3 pt-sm-0 pt-md-3 pt-lg-0">
													<h3 class="card-title h2 h3-sm h2-md pt-3">
														<a href="<?=$link?>">
															<?=$news->title?>
														</a>
													</h3>
													<div class="card-text mb-2 text-muted small">
														<time datetime="<?=$news->date?>">
															<?=date("d M, Y", strtotime($news->date))?>
														</time>
													</div>
													<a class="btn btn-outline-primary" href="<?=$link?>">Read more</a>
												</div>
											</div>
										</div>
									</article>
									
								<?php endforeach; ?>
							
                            </div>
                        </div>
						
						<?php if(0 == 1): ?>
							<div class="clearfix my-4">
								<nav class="float-left" aria-label="Page navigation example">
									<ul class="pagination">
										<li class="page-item active"><span class="page-link">1</span></li>
										<li class="page-item"><a class="page-link" href="../category/category.html">2</a></li>
										<li class="page-item"><a class="page-link" href="../category/category.html">3</a></li>
										<li class="page-item"><a class="page-link" href="../category/category.html">4</a></li>
										<li class="page-item"><span class="page-link disabled">....</span></li>
										<li class="page-item"><a class="page-link" href="../category/category.html">12</a></li>
										<li class="page-item">
											<a class="page-link" href="../category/category.html" aria-label="Next" title="Next page">
												<span aria-hidden="true"><i class="fas fa-chevron-right"></i></span>
												<span class="sr-only">Next</span>
											</a>
										</li>
									</ul>
								</nav>
								<span class="py-2 float-right">Page 1 of 12</span>
							</div>
						<?php endif; ?>
                    </div>
                    
                    <aside class="col-md-4 right-sidebar-lg">
                        <?php include("new-right-sidebar.php"); ?>
                    </aside>

                </div>
            </div>
        </div>
    </div>
</main>