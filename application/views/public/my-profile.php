<style>
.social-wallpaper{position:relative;height:300px;background:url(https://bootdey.com/img/Content/bg1.jpg) no-repeat;background-size:cover;background-color:#00b5ec}.card.social-tabs{border-top:none}.timeline-left .card,.timeline-right .card{border-top:none;box-shadow:0 0 1px 2px rgba(0,0,0,.05),0 -2px 1px -2px rgba(0,0,0,.04),0 0 0 -1px rgba(0,0,0,.05);transition:all 180ms linear}.timeline-left .card:hover,.timeline-right .card:hover{box-shadow:0 0 25px -5px #9e9c9e;transition:all 180ms linear}.timeline-icon{z-index:1}.tab-pane form .md-add-on i{font-size:20px}.wall-elips{position:absolute;right:15px}.social-profile{position:relative;padding-top:15px}.timeline-btn{position:absolute;bottom:0;right:30px}.nav-tabs.md-tabs.tab-timeline li a{padding:20px 0 10px;color:#666;font-size:18px}.social-timeline-left{position:absolute;top:-200px}.post-input{padding:10px 10px 10px 5px;display:block;width:100%;border:none;resize:none}.friend-box .media-object,.user-box .media-object{height:45px;width:45px;display:inline-block}.friend-box img{margin-right:10px;margin-bottom:10px}.user-box .media-left{position:relative}.chat-header{color:#222}.live-status{height:7px;width:7px;position:absolute;bottom:0;right:17px;border-radius:100%;border:1px solid}.tab-timeline .slide{bottom:-1px}.image-upload input{visibility:hidden;max-width:0;max-height:0}.file-upload-lbl{max-width:15px;padding:5px 0 0}.ellipsis::after{top:15px;border:none;position:absolute;content:'\f142';font-family:FontAwesome}.elipsis-box{box-shadow:0 0 5px 1px rgba(0,0,0,.11);top:40px;right:-10px}.elipsis-box:after{content:'';height:13px;width:13px;background:#fff;position:absolute;top:-5px;right:10px;-webkit-transform:rotate(45deg);-moz-transform:rotate(45deg);transform:rotate(45deg);box-shadow:-3px -3px 11px 1px rgba(170,170,170,.22)}.friend-elipsis{left:-10px;top:-10px}.social-profile:hover .profile-hvr,.social-wallpaper:hover .profile-hvr{opacity:1;transition:all ease-in-out .3s}.profile-hvr{opacity:0;position:absolute;text-align:center;width:100%;font-size:20px;padding:10px;top:0;color:#fff;background-color:rgba(0,0,0,.61);transition:all ease-in-out .3s}.social-profile{margin:0 15px}.social-follower{text-align:center}.social-follower h4{font-size:18px;margin-bottom:10px;font-style:normal}.social-follower h5{font-size:14px;font-weight:300}.social-follower .follower-counter{text-align:center;margin-top:25px;margin-bottom:25px;font-size:13px}.social-follower .follower-counter .txt-primary{font-size:24px}.timeline-icon{height:45px;width:45px;display:block;margin:0 auto;border:4px #fff solid}.social-timelines-left:after{height:3px;width:25%;position:absolute;background:#ccc;top:20px;content:"";right:0;z-index:-1}.social-timelines:before{position:absolute;content:' ';width:3px;background:#ccc;left:4%;z-index:-1;height:100%;top:0}.timeline-dot:after,.timeline-dot:before{content:"";position:absolute;height:9px;width:9px;background-color:#ccc;left:3.8%;border-radius:100%}.post-timelines .social-time,.user-box .social-designation{font-size:13px}.social-msg span{color:#666;padding-left:10px;padding-right:10px;margin-right:10px}.contact-info .social-label,.view-info .social-label,.work-info .social-label{font-size:15px;padding-left:0;padding-top:0}.contact-info .social-user-name,.view-info .social-user-name,.work-info .social-user-name{font-size:14px;padding-left:0}.friend-elipsis .social-designation{font-size:13px}.social-client-description{padding-bottom:20px}.timeline-details p{padding-top:10px}.post-timelines .chat-header,.timeline-details .chat-header{font-size:15px}.social-client-description{padding-bottom:20px}.social-client-description p{margin-top:5px}.social-client-description span{font-size:12px;margin-left:10px}.social-client-description .chat-header{font-size:13px}.social-tabs a{font-size:18px}.timeline-btn a{margin-bottom:20px}.profile-hvr i{cursor:pointer}.social-timelines:before{position:absolute;content:' ';width:3px;background:#ccc;left:4%;z-index:-1;height:100%;top:0}.timeline-dot:after,.timeline-dot:before{content:"";position:absolute;height:9px;width:9px;background-color:#ccc;left:3.8%;border-radius:100%}ul#profile-lightgallery{display:inline-flex}.social-timeline .btn i{margin-right:0}.card .card-block{padding:25px}.social-follower{text-align:center}.media-left{padding-right:20px}.live-status{height:9px;width:9px;position:absolute;bottom:0;right:17px;border-radius:100%;border:1px solid;top:5px}.live-status{height:10px;width:10px;position:absolute;top:20px;right:20px;border-radius:100%;border:1px solid}.bg-danger{background-color:#ff5370!important;color:#fff}.card{border-radius:5px;-webkit-box-shadow:0 1px 2.94px .06px rgba(4,26,55,.16);box-shadow:0 1px 2.94px .06px rgba(4,26,55,.16);border:none;margin-bottom:30px;-webkit-transition:all .3s ease-in-out;transition:all .3s ease-in-out}.friend-box .media-object,.user-box .media-object{height:30px;width:30px;display:inline-block;cursor:pointer}.md-tabs .nav-item{width:calc(100%/ 4);text-align:center}.md-tabs .nav-item .nav-link.active~.slide{opacity:1;-webkit-transition:all .3s ease-out;transition:all .3s ease-out}.md-tabs .nav-item .nav-link~.slide{opacity:0;-webkit-transition:all .3s ease-out;transition:all .3s ease-out}.tab-timeline .slide{bottom:-1px}.nav-tabs .slide{background:#4099ff;width:calc(100%/ 4);height:4px;position:absolute;-webkit-transition:left .3s ease-out;transition:left .3s ease-out;bottom:0}.mb--5{margin-bottom:5px}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div>
				<div class="content social-timeline">
					<div class="">

						<div class="row">
							<div class="col-md-12">
								<div class="social-wallpaper" style="background: url(<?=base_url()?>uploads/<?=$user_details->profile_banner_image?>) no-repeat;background-size: cover;">
									<div class="profile-hvr">
										<i class="icofont icofont-ui-edit p-r-10" onclick="$('input[name=profile-banner-image]').click()"></i>
									</div>
								</div>
								<div class="timeline-btn">
									<div class="hide">
										<form class="profile-banner-image-form" enctype="multipart/form-data">
											<input type="file" name="profile-banner-image" accept=".jpg,.jpeg" onchange="$('.profile-banner-image-form').submit();">
											<input type="hidden" name="profile-id" value="<?=$user_details->id?>">
										</form>
									</div>
								</div>

							</div>
						</div>

						<div class="row" style="min-height: 600px;">
							<div class="col-xl-3 col-lg-4 col-md-4 col-xs-12">
								<div class="social-timeline-left">
									<div class="card">
										<div class="social-profile">
											<img class="img-fluid width-100" src="<?=base_url()?>uploads/<?=$user_details->profile_image?>" alt="<?=base_url()?>uploads/<?=$user_details->profile_image?>">
											<div class="profile-hvr m-t-15">
												<i class="icofont icofont-ui-edit p-r-10" onclick="$('input[name=profile-image]').click()"></i>
											</div>
										</div>
										<div class="card-block social-follower">
											<h4><?=$login_name?></h4>
											<h5><?php echo $this->frontend_model->get_record("tbl_services", "id=" . $user_details->service, "name"); ?></h5>
											<div class="hide">
												<form class="profile-image-form" enctype="multipart/form-data">
													<input type="file" name="profile-image" accept=".jpg,.jpeg" onchange="$('.profile-image-form').submit();">
													<input type="hidden" name="profile-id" value="<?=$user_details->id?>">
												</form>
											</div>
										</div>
									</div>
									
									<div class="card">
										<div class="card-header">
											<h5 class="card-header-text d-inline-block">Recent Users</h5>
										</div>
										<div class="card-block friend-box">
											<?php foreach($this->frontend_model->get_records('tbl_general_users', "status = '0' order by id desc limit 20") as $user): ?>
												<a href="<?=base_url()?>profile/<?=$user->id?>/<?=$tis->slugify($user->first_name)?>-<?=$tis->slugify($user->last_name)?>">
													<img class="media-object img-radius" src="<?=base_url()?>uploads/<?=$user->profile_image?>" alt="<?=base_url()?>uploads/<?=$user->profile_image?>" title="<?=ucfirst($user->first_name)?> <?=ucfirst($user->last_name)?>">
												</a>
											<?php endforeach; ?>
										</div>
									</div>

								</div>

							</div>
							<div class="col-xl-9 col-lg-8 col-md-8 col-xs-12 ">

								<div class="card social-tabs">
									<ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#about" role="tab">About</a>
											<div class="slide"></div>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#photos" role="tab">Gallery</a>
											<div class="slide"></div>
										</li>
									</ul>
								</div>

								<div class="tab-content">

									<div class="tab-pane active" id="about">
										<div class="row">
											<div class="col-sm-12">
												<div class="card">
													<div class="card-header">
														<h5 class="card-header-text">Add your achievements / awards</h5>
														<button type="button" class="btn btn-primary waves-effect waves-light f-right" data-toggle="modal" data-target="#new-acheivement">
															<i class="fa fa-plus"></i>
														</button>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<?php foreach($this->frontend_model->get_records("tbl_user_acheivements", "status = '0' and user_id = '$_SESSION[login_id]'") as $user_ach): ?>
												<div class="col-sm-6">
													<div class="card">
														<div class="card-overflow-image">
															<img class="card-img-top w-100" src="<?=base_url()?>uploads/<?=$user_ach->proof_image?>" alt="<?=base_url()?>uploads/<?=$user_ach->proof_image?>">
														</div>
														<div class="card-body">
															<h4 class="card-title"><?=$user_ach->title?></h4>
															<p class="card-text"><?=$user_ach->position?></p>
														</div>
														<ul class="list-group list-group-flush bordertb-1">
															<li class="list-group-item"><b>Issuer:</b> <?=$user_ach->issued_by?></li>
															<li class="list-group-item"><b>Place:</b> <?=$user_ach->place?></li>
															<li class="list-group-item"><b>Date:</b> <?=$user_ach->date?></li>
														</ul>
														<div class="card-body">
															<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#delete-confirm" onclick="$('#delete-confirm form input[name=post-id]').val('<?=$user_ach->id?>');">
																Delete
															</button> 
														</div>
													</div>
												</div>
											<?php endforeach; ?>
										</div>
									</div>

									<div class="tab-pane" id="photos">
										<div class="card">
											<div class="card-block">
												<form class="user-gallery-images" enctype="multipart/form-data">
													<div class="input-images-1"></div>
													<button type="submit" class="btn btn-sm btn-primary">Submit</button>
												</form>
											</div>
										</div>
										<div class="card">
											<div class="card-block">
												<div class="row">
													<?php $i = 1; ?>
													<?php foreach($this->frontend_model->get_records("tbl_user_gallery", "status = '0' and user_id = '$_SESSION[login_id]'") as $user_gallery): ?>
														<div class="col-md-3">
															<a href="javascript: void(0);" onclick="openGalleryView();currentSlide(<?=$i?>);">
																<img src="<?=base_url()?>uploads/<?=$user_gallery->image?>" class="img-fluid" alt="<?=base_url()?>uploads/<?=$user_gallery->image?>">
															</a>
														</div>
														<?php if($i%4 == 0): ?>
														</div>
														<hr>
														<div class="row">
														<?php endif; ?>
														<?php $i++; ?>
													<?php endforeach; ?>
												</div>
											</div>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="delete-confirm" class="modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">
					Are you sure you want to delete your post?
				</h5>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="clearfix text-center">
							<form>
								<input type="hidden" name="post-id">
								<button type="submit" class="btn btn-primary">
									Delete
								</button>
								<button type="button" data-dismiss="modal" class="btn btn-danger">
									Cancel
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal" id="new-acheivement">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="add-acheivements" enctype="multipart/form-data">
				<div class="modal-header">
					<h4 class="modal-title">Add your acheivements / awards</h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<p class="form-row form-row-wide">
								<label>Acheivement / Award <sup class="text-danger">*</sup> </label>
								<input name="acheivement-award" type="text" placeholder="Eg: Rajiv Gandhi Khel Ratna Awards" required="required" class="form-control">
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="form-row form-row-wide">
								<label>Honoured for / Position <sup class="text-danger">*</sup> </label>
								<input name="position" type="text" placeholder="Eg: Best player of the series, Gold medal, First place" required="required" class="form-control">
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="form-row form-row-wide">
								<label>Issued by <sup class="text-danger">*</sup> </label>
								<input name="issuer" type="text" placeholder="Eg: Institute name, Person name, Organization name" required="required" class="form-control">
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="form-row form-row-wide">
								<label>Event conducted Date <sup class="text-danger">*</sup></label>
								<input name="date" type="date" value="<?=date('Y-m-d')?>" required="required" class="form-control">
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<p class="form-row form-row-wide">
								<label>Event conducted place <sup class="text-danger">*</sup> </label>
								<input name="place" type="text" placeholder="Eg: Chennai, Institute name, stadium name, academy name" required="required" class="form-control">
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Proof image <sup class="text-danger">*</sup></label><br>
							<input name="proof" type="file" required="required"accept=".jpg,.jpeg">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="custom-light-box">
	<div id="gallery-view" class="modal">
		<span class="close cursor" onclick="closeGalleryView()">&times;</span>
		<div class="modal-content">
			<?php foreach($this->frontend_model->get_records("tbl_user_gallery", "status = '0' and user_id = '$_SESSION[login_id]'") as $user_gallery): ?>
				<div class="mySlides text-center">
					<img src="<?=base_url()?>uploads/<?=$user_gallery->image?>" alt="<?=base_url()?>uploads/<?=$user_gallery->image?>">
				</div>
			<?php endforeach; ?>
			
			<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
			<a class="next" onclick="plusSlides(1)">&#10095;</a>

			<div class="row mt-25">
				<?php $i = 1; ?>
				<?php foreach($this->frontend_model->get_records("tbl_user_gallery", "status = '0' and user_id = '$_SESSION[login_id]'") as $user_gallery): ?>
					<div class="col-md-1">
						<img class="demo cursor" src="<?=base_url()?>uploads/<?=$user_gallery->image?>" style="width:100%" onclick="currentSlide(<?=$i?>)" alt="<?=base_url()?>uploads/<?=$user_gallery->image?>">
					</div>
					<?php $i++; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>

<script>
function openGalleryView() {
	document.getElementById("gallery-view").style.display = "block";
}

function closeGalleryView() {
	document.getElementById("gallery-view").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
	showSlides(slideIndex += n);
}

function currentSlide(n) {
	showSlides(slideIndex = n);
}	

function showSlides(n) {
	var i;
	var slides = document.getElementsByClassName("mySlides");
	var dots = document.getElementsByClassName("demo");
	if (n > slides.length) {slideIndex = 1}
	if (n < 1) {slideIndex = slides.length}
	for (i = 0; i < slides.length; i++) {
		slides[i].style.display = "none";
	}
	for (i = 0; i < dots.length; i++) {
		dots[i].className = dots[i].className.replace(" active", "");
	}
	slides[slideIndex-1].style.display = "block";
	dots[slideIndex-1].className += " active";
}
</script>
<link href="<?=base_url()?>assets/front/css/img-uploader.css" rel="stylesheet" />
<script src="<?=base_url()?>assets/front/js/img-uploader.js"></script>
<script>
	$(function () {
		$('.input-images-1').imageUploader({
			imagesInputName: 'gallery_image',
			extensions: ['.jpg','.jpeg','.png','.gif','.JPG','.JPEG','.PNG','.GIF'],
			mimes: ['image/jpeg','image/jpg','image/png','image/gif'],
		});
	});
</script>